#!/bin/python
import datetime
import notify2
import time
import glob
import os

print("hello there");

mainID=93997594

logsDir = os.path.expanduser("~")+"/.steam/steam/steamapps/compatdata/8500/pfx/drive_c/users/steamuser/My Documents/EVE/logs/Chatlogs/";

chats = [
	"munstest",
	"local",
	"d-land",
	"beans",
	"corp",
	"fleet",
	"munsies"
];

systems = [
	b'Q-VTWJ',
	b'HB-5L3',
	b'VY-866',
	b'RGU1-T',
	b'MO-YDG',
	b'N4TD-6',
	b'42SU-L',
	b'D9Z-VY',
	b'1GT-MA',
	b'4LNE-M',
	b'DK0-N8',
	b'4GSZ-1',
	b'HXK-J6',
	b'U6R-F9',
	b'L-Z9NB',
	b'L-Z9NB',
	b'EJ-5X2',
	b'IAMJ-Q',
	b'CUT-0V',
	b'J-QOKQ',
	b'4GSZ-1',
	b'9-WEMC',
	b'E0DR-G'
];

myDir = os.path.dirname(os.path.realpath(__file__))
notify2.init("EvE Alerter");
alert=notify2.Notification("eve alerter","started",myDir+"/alert.png");
alert.set_urgency(notify2.URGENCY_CRITICAL);
alert.set_timeout(5000);
alert.show();
today= datetime.date.today().strftime("%Y%m%d");

files = glob.glob(logsDir + "*"+today+"*"+str(mainID)+"*");

chatFiles = []

for file in files:
	print(file)
	if any(chat.lower() in file.lower() for chat in chats):
		print(file);
		tmp=open(file,"rb");
		tmp.read();
		chatFiles.append(tmp)

while True:
	for f in chatFiles:
		line=f.read().replace(b'\x00',b'');
		if line != b'':
			txt=line[line.find(b' >')+2:].rstrip(b'\r\n');
			if any(system.lower() in txt.lower() for system in systems):
				alert.update("SYSTEM MENTIONED",txt);
				alert.show();
				os.system("mpg123 "+myDir+"/alert.mp3 &");
			
	time.sleep(0.1)
